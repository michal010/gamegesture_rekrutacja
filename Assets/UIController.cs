﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public TextMeshProUGUI NotificationText;
    public List<Toggle> soundToggles;
    public AudioClip[] audioClips;
    private AudioSource source;

    float NotificationTextTime = 2f;

    private void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    public void OpenUIElement(GameObject uiElement)
    {
        Animator anim = uiElement.GetComponent<Animator>();
        anim.gameObject.SetActive(true);
        anim.SetBool("open", true);
    }
    public void CloseUIElement(GameObject uiElement)
    {
        Animator anim = uiElement.GetComponent<Animator>();
        uiElement.SetActive(false);
        anim.SetBool("open", false);
    }

    public void ShowTimedText(string text)
    {
        if (NotificationText.gameObject.activeSelf)
            return;
        NotificationText.text = text;
        StartCoroutine(ShowTimedTextCoroutine(NotificationTextTime));
    }

    IEnumerator ShowTimedTextCoroutine(float time)
    {
        NotificationText.gameObject.SetActive(true);
        yield return new WaitForSeconds(time);
        NotificationText.gameObject.SetActive(false);
    }



    public void PlaySound()
    {
        List<AudioClip> possibleClips = new List<AudioClip>();
        for (int i = 0; i < soundToggles.Count(); i++)
        {
            if (soundToggles[i].isOn)
                possibleClips.Add(audioClips[i]);
        }

        if(possibleClips.Count() > 0)
        {
            int RandomNumber = Random.Range(0, possibleClips.Count());
            source.clip = possibleClips[RandomNumber];
            source.Play();
        }
    }
}
